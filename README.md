# Minimal node server
Un servidor en Node mínimo que implementa lo necesario para una API REST funcional, implementa los métodos de
POST y GET como ejemplos, pueden usarse PUT, DELETE, etc siguiendo el mismo formato.
<br>
Este ejemplo utiliza postgres como base de datos, puede ser fácilmente cambiada por cualquier otra base de datos como Oracle o
Mysql.
<br>
Las tablas, información que reciben y devuelven los endpoints no tienen ningún valor de importancia. Solo están como placeholders
y deben ser cambiados por la implementacioń real de los endpoints.
<br>
La variable de entorno para definir el puerto es PORT y basta con establecer  esta variable de entorno para especificar donde escuchará 
el servidor. De lo contrario utiliza el puerto 1989 por default.
<br>
La funcion wrap recibe dos argumentos: Una función y un mensaje string opcional que será impreso en consola cada vez que el método
sea invocado. Esto puede ser útil para hacer un debugging. El método wrap se encarga de ejecutar la función pasada como argumento
de manera segura, ya que en caso de ocurrir un error que no fué tomado en cuenta, este atrapa el error y envía una respuesta HTTP
acorde. En vez de detener la ejecución por culpa de una unhandled exception.
<br>
Los archivos server.js y wrapper.js no es necesario y no se recomienda que los editen. Los endpoints deben ser implementados en project.js.
De ser necesaria una estructura más compleja, pueden definir los endpoints en módulos separados e importarlos en project.js para linkearlos
al router.
<br>
El punto de entrada es server.js no index.js. Para correr el servidor basta con ejecutar el comando:
```zsh
node server.js
```
