const express = require("express");
const wrap_function = require("./wrapper.js");
const router = express.Router();
const { Pool } = require('pg'); //Si estan usando Mysql, cambiar pg por la de mysql. pg es para Postgres btw.

const pool = new Pool();

const Query = async function ( sql, params ) {
   return (await pool.query(sql,params)).rows;
}

async function get_data(req,res) {
  return res.send({
    success: true,
    data : (await pool.query('select * from overview($1)',[reposo]))
    .rows[0]
  });
}

async function digest(req, res) {
  const ren = (await pool.query('select * from register_point($1,$2,$3)',[
    req.body.estado,
    req.body.peso,
    req.body.ubicacion
  ])).rows;
  return res.send({ success: true });
}

router.get("/project/data", wrap_function(get_data)); //Ejemplo GET
router.post("/project/data", wrap_function(digest)); //Ejemplo POST

module.exports = router
