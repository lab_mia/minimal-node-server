function wrap_function(anotherFunction,debugInfo=null) {
  return function (req, res) {
	  if(debugInfo)console.log(Array.isArray(debugInfo)?`${debugInfo[0]} - ${debugInfo[1]}`:debugInfo.toString());
    anotherFunction(req, res).catch((err) => {
      return res.send({
        success: false,
        message: err["message"] ? err["message"] : "An error occurred.",
      });
    });
  };
}
module.exports = wrap_function;
